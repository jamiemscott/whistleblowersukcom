<?php

/**
 * [whistleblowers_preprocess_page description]
 * @param  [type] $vars [description]
 * @return [type]       [description]
 */
function whistleblowers_preprocess_page(&$vars) {

  $custom_css = drupal_get_path('theme', 'whistleblowers') ."/assets/css/custom.css";

  drupal_add_css(
    $custom_css,
     array(
       'group' => CSS_THEME,
       'type' => 'file',
       'media' => 'all',
       'preprocess' => TRUE,
       'weight' => '9999',
     )
   );

}

function whistleblowers_menu_tree__main_menu(&$variables) {
  return '<nav id="tb-menu-main-menu" class="nav-menu"><ul class="tb-menu main-menu">' . $variables['tree'] . '</ul></nav>';
}
