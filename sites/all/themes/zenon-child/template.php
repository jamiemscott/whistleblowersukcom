<?php

/**
 * [zenon_child_preprocess_page description]
 * @param  [type] $vars [description]
 * @return [type]       [description]
 */
function zenon_child_preprocess_page(&$vars) {

  $custom_css = drupal_get_path('theme', 'zenon_child') ."/custom.css";

  drupal_add_css(
    $custom_css,
     array(
       'group' => CSS_THEME,
       'type' => 'file',
       'media' => 'all',
       'preprocess' => TRUE,
       'weight' => '9999',
     )
   );

}
