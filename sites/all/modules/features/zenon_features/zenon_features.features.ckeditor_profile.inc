<?php
/**
 * @file
 * zenon_features.features.ckeditor_profile.inc
 */

/**
 * Implements hook_ckeditor_profile_defaults().
 */
function zenon_features_ckeditor_profile_defaults() {
  $data = array(
    'Advanced' => array(
      'name' => 'Advanced',
      'settings' => array(
        'ss' => 2,
        'toolbar' => '[
    [\'Source\'],
    [\'Cut\',\'Copy\',\'Paste\',\'PasteText\',\'PasteFromWord\',\'-\',\'SpellChecker\',\'Scayt\'],
    [\'Undo\',\'Redo\',\'Find\',\'Replace\',\'-\',\'SelectAll\'],
    [\'Image\',\'Flash\',\'Table\',\'HorizontalRule\',\'Smiley\',\'SpecialChar\'],
    [\'Maximize\',\'ShowBlocks\',\'TransformTextToLowercase\',\'TransformTextToUppercase\',\'TransformTextCapitalize\',\'FontAwesome\',\'Glyphicons\',\'SimpleLineicons\'],
    \'/\',
    [\'Bold\',\'Italic\',\'Underline\',\'Strike\',\'-\',\'Subscript\',\'Superscript\',\'-\',\'RemoveFormat\'],
    [\'NumberedList\',\'BulletedList\',\'-\',\'Outdent\',\'Indent\',\'Blockquote\'],
    [\'JustifyLeft\',\'JustifyCenter\',\'JustifyRight\',\'JustifyBlock\',\'-\',\'BidiLtr\',\'BidiRtl\'],
    [\'Link\',\'Unlink\',\'Anchor\']
]',
        'expand' => 't',
        'default' => 't',
        'show_toggle' => 't',
        'uicolor' => 'default',
        'uicolor_user' => 'default',
        'width' => '100%',
        'lang' => 'en',
        'auto_lang' => 't',
        'language_direction' => 'default',
        'allowed_content' => 't',
        'extraAllowedContent' => '',
        'enter_mode' => 'p',
        'shift_enter_mode' => 'br',
        'font_format' => 'p;div;pre;address;h1;h2;h3;h4;h5;h6',
        'custom_formatting' => 'f',
        'formatting' => array(
          'custom_formatting_options' => array(
            'indent' => 'indent',
            'breakBeforeOpen' => 'breakBeforeOpen',
            'breakAfterOpen' => 'breakAfterOpen',
            'breakAfterClose' => 'breakAfterClose',
            'breakBeforeClose' => 0,
            'pre_indent' => 0,
          ),
        ),
        'css_mode' => 'none',
        'css_path' => '',
        'css_style' => 'theme',
        'styles_path' => '',
        'filebrowser' => 'none',
        'filebrowser_image' => '',
        'filebrowser_flash' => '',
        'UserFilesPath' => '%b%f/',
        'UserFilesAbsolutePath' => '%d%b%f/',
        'forcePasteAsPlainText' => 'f',
        'html_entities' => 'f',
        'scayt_autoStartup' => 'f',
        'theme_config_js' => 't',
        'js_conf' => '',
        'loadPlugins' => array(
          'drupalbreaks' => array(
            'name' => 'drupalbreaks',
            'desc' => 'Plugin for inserting Drupal teaser and page breaks.',
            'path' => '%plugin_dir%drupalbreaks/',
            'buttons' => array(
              'DrupalBreak' => array(
                'label' => 'DrupalBreak',
                'icon' => 'images/drupalbreak.png',
              ),
            ),
            'default' => 't',
          ),
          'fontawesome' => array(
            'name' => 'fontawesome',
            'desc' => 'Plugin file: fontawesome',
            'path' => '%plugin_dir_extra%fontawesome/',
            'buttons' => array(
              'FontAwesome' => array(
                'label' => 'FontAwesome',
                'icon' => 'images/fontawesome.png',
              ),
            ),
            'default' => 'f',
          ),
          'glyphicons' => array(
            'name' => 'glyphicons',
            'desc' => 'Plugin file: glyphicons',
            'path' => '%plugin_dir_extra%glyphicons/',
            'buttons' => array(
              'Glyphicons' => array(
                'label' => 'Glyphicons',
                'icon' => 'images/glyphicons.png',
              ),
            ),
            'default' => 'f',
          ),
          'simplelineicons' => array(
            'name' => 'simplelineicons',
            'desc' => 'Plugin file: simplelineicons',
            'path' => '%plugin_dir_extra%simplelineicons/',
            'buttons' => array(
              'SimpleLineicons' => array(
                'label' => 'SimpleLineicons',
                'icon' => 'images/simplelineicons.png',
              ),
            ),
            'default' => 'f',
          ),
        ),
      ),
      'input_formats' => array(
        'filtered_html' => 'Filtered HTML',
      ),
    ),
    'Basic' => array(
      'name' => 'Basic',
      'settings' => array(
        'ss' => 2,
        'toolbar' => '[
    [\'Bold\',\'Italic\',\'Underline\',\'Strike\',\'-\',\'NumberedList\',\'BulletedList\',\'-\',\'Superscript\',\'Subscript\',\'-\',\'Blockquote\',\'-\',\'RemoveFormat\']
]',
        'expand' => 't',
        'default' => 't',
        'show_toggle' => 't',
        'uicolor' => 'default',
        'uicolor_user' => 'default',
        'width' => '100%',
        'lang' => 'en',
        'auto_lang' => 't',
        'language_direction' => 'default',
        'allowed_content' => 't',
        'extraAllowedContent' => '',
        'enter_mode' => 'p',
        'shift_enter_mode' => 'br',
        'font_format' => 'p;div;pre;address;h1;h2;h3;h4;h5;h6',
        'custom_formatting' => 'f',
        'formatting' => array(
          'custom_formatting_options' => array(
            'indent' => 'indent',
            'breakBeforeOpen' => 'breakBeforeOpen',
            'breakAfterOpen' => 'breakAfterOpen',
            'breakAfterClose' => 'breakAfterClose',
            'breakBeforeClose' => 0,
            'pre_indent' => 0,
          ),
        ),
        'css_mode' => 'theme',
        'css_path' => '',
        'css_style' => 'theme',
        'styles_path' => '',
        'filebrowser' => 'none',
        'filebrowser_image' => '',
        'filebrowser_flash' => '',
        'UserFilesPath' => '%b%f/',
        'UserFilesAbsolutePath' => '%d%b%f/',
        'forcePasteAsPlainText' => 'f',
        'html_entities' => 'f',
        'scayt_autoStartup' => 'f',
        'theme_config_js' => 'f',
        'js_conf' => '',
        'loadPlugins' => array(
          'drupalbreaks' => array(
            'name' => 'drupalbreaks',
            'desc' => 'Plugin for inserting Drupal teaser and page breaks.',
            'path' => '%plugin_dir%drupalbreaks/',
            'buttons' => array(
              'DrupalBreak' => array(
                'label' => 'DrupalBreak',
                'icon' => 'images/drupalbreak.png',
              ),
            ),
            'default' => 't',
          ),
        ),
      ),
      'input_formats' => array(
        'comments' => 'Comments',
      ),
    ),
    'CKEditor Global Profile' => array(
      'name' => 'CKEditor Global Profile',
      'settings' => array(
        'skin' => 'bootstrapck',
        'ckeditor_path' => '%l/ckeditor',
        'ckeditor_local_path' => '',
        'ckeditor_plugins_path' => '%l/ckeditor/plugins',
        'ckeditor_plugins_local_path' => '',
        'ckfinder_path' => '%m/ckfinder',
        'ckfinder_local_path' => '',
        'ckeditor_aggregate' => 'f',
        'toolbar_wizard' => 't',
        'loadPlugins' => array(),
      ),
      'input_formats' => array(),
    ),
    'Full' => array(
      'name' => 'Full',
      'settings' => array(
        'ss' => 2,
        'toolbar' => '[
    [\'Source\'],
    [\'Cut\',\'Copy\',\'Paste\',\'PasteText\',\'PasteFromWord\',\'-\',\'SpellChecker\',\'Scayt\'],
    [\'Undo\',\'Redo\',\'Find\',\'Replace\',\'-\',\'SelectAll\'],
    [\'Image\',\'Media\',\'Flash\',\'Table\',\'HorizontalRule\',\'Smiley\',\'SpecialChar\',\'Iframe\',\'-\',\'TransformTextSwitcher\',\'TransformTextToUppercase\',\'TransformTextToLowercase\',\'TransformTextCapitalize\',\'FontAwesome\',\'Glyphicons\',\'SimpleLineicons\'],
    \'/\',
    [\'Bold\',\'Italic\',\'Underline\',\'Strike\',\'-\',\'Subscript\',\'Superscript\',\'-\',\'RemoveFormat\'],
    [\'NumberedList\',\'BulletedList\',\'-\',\'Outdent\',\'Indent\',\'Blockquote\',\'CreateDiv\'],
    [\'JustifyLeft\',\'JustifyCenter\',\'JustifyRight\',\'JustifyBlock\',\'-\',\'BidiLtr\',\'BidiRtl\',\'-\',\'Language\'],
    [\'Link\',\'Unlink\',\'Anchor\'],
    [\'DrupalBreak\'],
    \'/\',
    [\'Styles\',\'Format\',\'Font\',\'FontSize\'],
    [\'TextColor\',\'BGColor\'],
    [\'Maximize\',\'ShowBlocks\',\'leaflet\',\'Video\']
]',
        'expand' => 't',
        'default' => 't',
        'show_toggle' => 't',
        'uicolor' => 'default',
        'uicolor_user' => 'default',
        'width' => '100%',
        'lang' => 'en',
        'auto_lang' => 't',
        'language_direction' => 'default',
        'allowed_content' => 't',
        'extraAllowedContent' => '',
        'enter_mode' => 'p',
        'shift_enter_mode' => 'br',
        'font_format' => 'p;div;pre;address;h1;h2;h3;h4;h5;h6',
        'custom_formatting' => 't',
        'formatting' => array(
          'custom_formatting_options' => array(
            'indent' => 'indent',
            'breakBeforeOpen' => 'breakBeforeOpen',
            'breakAfterOpen' => 'breakAfterOpen',
            'breakBeforeClose' => 'breakBeforeClose',
            'breakAfterClose' => 'breakAfterClose',
            'pre_indent' => 0,
          ),
        ),
        'css_mode' => 'self',
        'css_path' => '%t/assets/css/zenon.css,%hsites/all/libraries/fontawesome/css/font-awesome.min.css',
        'css_style' => 'theme',
        'styles_path' => '',
        'filebrowser' => 'none',
        'filebrowser_image' => 'imce',
        'filebrowser_flash' => '',
        'UserFilesPath' => '%b%f/',
        'UserFilesAbsolutePath' => '%d%b%f/',
        'forcePasteAsPlainText' => 'f',
        'html_entities' => 'f',
        'scayt_autoStartup' => 'f',
        'theme_config_js' => 't',
        'js_conf' => 'config.extraPlugins = \'codemirror,simplelineicons,glyphicons,fontawesome,texttransform,widget\';',
        'loadPlugins' => array(
          'codemirror' => array(
            'name' => 'codemirror',
            'desc' => 'Plugin file: codemirror',
            'path' => '%plugin_dir_extra%codemirror/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'drupalbreaks' => array(
            'name' => 'drupalbreaks',
            'desc' => 'Plugin for inserting Drupal teaser and page breaks.',
            'path' => '%plugin_dir%drupalbreaks/',
            'buttons' => array(
              'DrupalBreak' => array(
                'label' => 'DrupalBreak',
                'icon' => 'images/drupalbreak.png',
              ),
            ),
            'default' => 't',
          ),
          'fontawesome' => array(
            'name' => 'fontawesome',
            'desc' => 'Plugin file: fontawesome',
            'path' => '%plugin_dir_extra%fontawesome/',
            'buttons' => array(
              'FontAwesome' => array(
                'label' => 'FontAwesome',
                'icon' => 'images/fontawesome.png',
              ),
            ),
            'default' => 'f',
          ),
          'glyphicons' => array(
            'name' => 'glyphicons',
            'desc' => 'Plugin file: glyphicons',
            'path' => '%plugin_dir_extra%glyphicons/',
            'buttons' => array(
              'Glyphicons' => array(
                'label' => 'Glyphicons',
                'icon' => 'images/glyphicons.png',
              ),
            ),
            'default' => 'f',
          ),
          'leaflet' => array(
            'name' => 'leaflet',
            'desc' => 'Plugin file: leaflet',
            'path' => '%plugin_dir_extra%leaflet/',
            'buttons' => array(
              'leaflet' => array(
                'label' => 'leaflet',
                'icon' => 'icons/leaflet.png',
              ),
            ),
            'default' => 'f',
          ),
          'lineutils' => array(
            'name' => 'lineutils',
            'desc' => 'Plugin file: lineutils',
            'path' => '%plugin_dir_extra%lineutils/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'richcombo' => array(
            'name' => 'richcombo',
            'desc' => 'Plugin file: richcombo',
            'path' => '%plugin_dir_extra%richcombo/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'simplelineicons' => array(
            'name' => 'simplelineicons',
            'desc' => 'Plugin file: simplelineicons',
            'path' => '%plugin_dir_extra%simplelineicons/',
            'buttons' => array(
              'SimpleLineicons' => array(
                'label' => 'SimpleLineicons',
                'icon' => 'images/simplelineicons.png',
              ),
            ),
            'default' => 'f',
          ),
          'stylescombo' => array(
            'name' => 'stylescombo',
            'desc' => 'Plugin file: stylescombo',
            'path' => '%plugin_dir_extra%stylescombo/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'video' => array(
            'name' => 'video',
            'desc' => 'Video plugin for CKEditor',
            'path' => '%plugin_dir_extra%video/',
            'buttons' => array(
              'Video' => array(
                'label' => 'Video',
                'icon' => 'images/icon.png',
              ),
            ),
            'default' => 'f',
          ),
          'widget' => array(
            'name' => 'widget',
            'desc' => 'Plugin file: widget',
            'path' => '%plugin_dir_extra%widget/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
        ),
      ),
      'input_formats' => array(
        'full_html' => 'Full HTML',
      ),
    ),
  );
  return $data;
}
