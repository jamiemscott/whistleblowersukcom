<?php
/**
 * @file
 * zenon_features.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function zenon_features_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_client_details|node|client|form';
  $field_group->group_name = 'group_client_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'client';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Client Details',
    'weight' => '0',
    'children' => array(
      0 => 'field_client_logo',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-client-details field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_client_details|node|client|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_customer_details|node|testimonials|form';
  $field_group->group_name = 'group_customer_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'testimonials';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Customer Details',
    'weight' => '0',
    'children' => array(
      0 => 'field_testimonials_author_name',
      1 => 'field_testimonials_author_title',
      2 => 'field_testimonials_author_photo',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-customer-details field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_customer_details|node|testimonials|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_details|node|portfolio|full';
  $field_group->group_name = 'group_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'portfolio';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Portfolio Details',
    'weight' => '14',
    'children' => array(
      0 => 'field_portfolio_category',
      1 => 'field_portfolio_url',
      2 => 'field_portfolio_client',
      3 => 'field_portfolio_date',
      4 => 'field_portfolio_skills',
    ),
    'format_type' => 'table',
    'format_settings' => array(
      'label' => 'Portfolio Details',
      'instance_settings' => array(
        'label_visibility' => '1',
        'desc' => '',
        'first_column' => '',
        'second_column' => '',
        'empty_label_behavior' => '2',
        'table_row_striping' => 0,
        'always_show_field_label' => 0,
        'classes' => 'group-details field-group-table',
      ),
    ),
  );
  $export['group_details|node|portfolio|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_faq_details|node|faq|form';
  $field_group->group_name = 'group_faq_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'faq';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'FAQ Destails',
    'weight' => '0',
    'children' => array(
      0 => 'field_faq_category',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-faq-details field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_faq_details|node|faq|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_member_details|node|team_members|form';
  $field_group->group_name = 'group_member_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'team_members';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Member Details',
    'weight' => '0',
    'children' => array(
      0 => 'field_member_photo',
      1 => 'field_team_membership',
      2 => 'field_member_social_links',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-member-details field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_member_details|node|team_members|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_plan_details|node|pricing_plan|form';
  $field_group->group_name = 'group_plan_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'pricing_plan';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Plan details',
    'weight' => '0',
    'children' => array(
      0 => 'field_subscription_type',
      1 => 'field_pricing_plan_price',
      2 => 'field_subscription_link',
      3 => 'field_pricing_plans_category',
      4 => 'field_pricing_plan_weight',
      5 => 'field_pricing_plan_highlighted',
      6 => 'field_pricing_plan_hover_text',
      7 => 'field_pricing_plan_color_scheme',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-plan-details field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_plan_details|node|pricing_plan|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_portfolio_assets|node|portfolio|form';
  $field_group->group_name = 'group_portfolio_assets';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'portfolio';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Portfolio Assets',
    'weight' => '6',
    'children' => array(
      0 => 'field_portfolio_images',
      1 => 'field_portfolio_preview_image',
      2 => 'field_portfolio_video',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Portfolio Assets',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_portfolio_assets|node|portfolio|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_portfolio_details|node|portfolio|form';
  $field_group->group_name = 'group_portfolio_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'portfolio';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Portfolio Details',
    'weight' => '7',
    'children' => array(
      0 => 'field_portfolio_category',
      1 => 'field_portfolio_url',
      2 => 'field_portfolio_client',
      3 => 'field_portfolio_date',
      4 => 'field_portfolio_skills',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Portfolio Details',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-portfolio-details field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_portfolio_details|node|portfolio|form'] = $field_group;

  return $export;
}
