<?php
/**
 * @file
 * zenon_features.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function zenon_features_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['delta_blocks-breadcrumb'] = array(
    'cache' => 4,
    'custom' => 0,
    'delta' => 'breadcrumb',
    'module' => 'delta_blocks',
    'node_types' => array(),
    'pages' => '<front>
home
home/*',
    'roles' => array(),
    'tb_blocks' => 'a:9:{s:5:"colxs";s:2:"12";s:5:"colsm";s:2:"12";s:5:"colmd";s:1:"4";s:5:"collg";s:1:"4";s:7:"visible";s:0:"";s:6:"hidden";a:2:{i:0;s:2:"xs";i:1;s:2:"sm";}s:9:"animation";s:0:"";s:5:"delay";s:0:"";s:14:"custom_classes";s:27:"breadcrumb-naked text-right";}',
    'themes' => array(
      'zenon' => array(
        'region' => 'static_page_title',
        'status' => 1,
        'theme' => 'zenon',
        'weight' => -14,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['delta_blocks-logo'] = array(
    'cache' => 8,
    'custom' => 0,
    'delta' => 'logo',
    'module' => 'delta_blocks',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'tb_blocks' => 'a:9:{s:5:"colxs";s:2:"12";s:5:"colsm";s:2:"12";s:5:"colmd";s:1:"2";s:5:"collg";s:1:"2";s:7:"visible";a:0:{}s:6:"hidden";a:0:{}s:9:"animation";s:0:"";s:5:"delay";s:0:"";s:14:"custom_classes";s:55:"text-center-xs text-center-sm text-left-lg text-left-md";}',
    'themes' => array(
      'zenon' => array(
        'region' => 'navigation',
        'status' => 1,
        'theme' => 'zenon',
        'weight' => -36,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['delta_blocks-page-title'] = array(
    'cache' => 4,
    'custom' => 0,
    'delta' => 'page-title',
    'module' => 'delta_blocks',
    'node_types' => array(),
    'pages' => 'home/*',
    'roles' => array(),
    'tb_blocks' => 'a:9:{s:5:"colxs";s:2:"12";s:5:"colsm";s:2:"12";s:5:"colmd";s:1:"8";s:5:"collg";s:1:"8";s:7:"visible";s:0:"";s:6:"hidden";s:0:"";s:9:"animation";s:0:"";s:5:"delay";s:0:"";s:14:"custom_classes";s:0:"";}',
    'themes' => array(
      'zenon' => array(
        'region' => 'static_page_title',
        'status' => 1,
        'theme' => 'zenon',
        'weight' => -15,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['search-form'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'form',
    'module' => 'search',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'tb_blocks' => 'a:9:{s:5:"colxs";s:2:"12";s:5:"colsm";s:2:"12";s:5:"colmd";s:1:"3";s:5:"collg";s:1:"4";s:7:"visible";s:0:"";s:6:"hidden";s:0:"";s:9:"animation";s:0:"";s:5:"delay";s:0:"";s:14:"custom_classes";s:15:"margin-top-10px";}',
    'themes' => array(
      'zenon' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zenon',
        'weight' => -34,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'tb_blocks' => 'a:9:{s:5:"colxs";s:2:"12";s:5:"colsm";s:2:"12";s:5:"colmd";s:2:"12";s:5:"collg";s:2:"12";s:7:"visible";s:0:"";s:6:"hidden";s:0:"";s:9:"animation";s:0:"";s:5:"delay";s:0:"";s:14:"custom_classes";s:0:"";}',
    'themes' => array(
      'zenon' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'zenon',
        'weight' => -27,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'tb_blocks' => 'a:9:{s:5:"colxs";s:2:"12";s:5:"colsm";s:2:"12";s:5:"colmd";s:2:"12";s:5:"collg";s:2:"12";s:7:"visible";s:0:"";s:6:"hidden";s:0:"";s:9:"animation";s:0:"";s:5:"delay";s:0:"";s:14:"custom_classes";s:0:"";}',
    'themes' => array(
      'zenon' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zenon',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-navigation'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'navigation',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'tb_blocks' => 'a:9:{s:5:"colxs";s:2:"12";s:5:"colsm";s:2:"12";s:5:"colmd";s:2:"12";s:5:"collg";s:2:"12";s:7:"visible";s:0:"";s:6:"hidden";s:0:"";s:9:"animation";s:0:"";s:5:"delay";s:0:"";s:14:"custom_classes";s:0:"";}',
    'themes' => array(
      'zenon' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zenon',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-powered-by'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'powered-by',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'tb_blocks' => 'a:9:{s:5:"colxs";s:2:"12";s:5:"colsm";s:2:"12";s:5:"colmd";s:1:"6";s:5:"collg";s:1:"6";s:7:"visible";s:0:"";s:6:"hidden";s:0:"";s:9:"animation";s:0:"";s:5:"delay";s:0:"";s:14:"custom_classes";s:43:"text-center-xs text-center-sm text-right-md";}',
    'themes' => array(
      'zenon' => array(
        'region' => 'footer_bottom',
        'status' => 1,
        'theme' => 'zenon',
        'weight' => 10,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['tb_menu-0'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 0,
    'module' => 'tb_menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'tb_blocks' => 'a:9:{s:5:"colxs";s:2:"12";s:5:"colsm";s:1:"6";s:5:"colmd";s:2:"10";s:5:"collg";s:2:"10";s:7:"visible";a:0:{}s:6:"hidden";a:0:{}s:9:"animation";s:0:"";s:5:"delay";s:0:"";s:14:"custom_classes";s:15:"menu-pull-right";}',
    'themes' => array(
      'zenon' => array(
        'region' => 'navigation',
        'status' => 1,
        'theme' => 'zenon',
        'weight' => -35,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['user-login'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'login',
    'module' => 'user',
    'node_types' => array(),
    'pages' => 'page-both-sidebars
blogs/with-sidebar-second
blog/*',
    'roles' => array(),
    'tb_blocks' => 'a:9:{s:5:"colxs";s:2:"12";s:5:"colsm";s:2:"12";s:5:"colmd";s:2:"12";s:5:"collg";s:2:"12";s:7:"visible";s:0:"";s:6:"hidden";s:0:"";s:9:"animation";s:0:"";s:5:"delay";s:0:"";s:14:"custom_classes";s:4:"well";}',
    'themes' => array(
      'zenon' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'zenon',
        'weight' => -35,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
