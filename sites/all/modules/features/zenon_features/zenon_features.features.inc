<?php
/**
 * @file
 * zenon_features.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function zenon_features_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function zenon_features_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function zenon_features_image_default_styles() {
  $styles = array();

  // Exported image style: 1140x640.
  $styles['1140x640'] = array(
    'label' => '1140x640',
    'effects' => array(
      10 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1140,
          'height' => 640,
          'retinafy' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: 128x128.
  $styles['128x128'] = array(
    'label' => '128x128',
    'effects' => array(
      15 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 128,
          'height' => 128,
          'retinafy' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: 640x480.
  $styles['640x480'] = array(
    'label' => '640x480',
    'effects' => array(
      9 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 640,
          'height' => 480,
          'retinafy' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: 740x415.
  $styles['740x415'] = array(
    'label' => '740x415',
    'effects' => array(
      13 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 740,
          'height' => 415,
          'retinafy' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: 768x512.
  $styles['768x512'] = array(
    'label' => '768x512',
    'effects' => array(
      27 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 768,
          'height' => 695,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: avatar.
  $styles['avatar'] = array(
    'label' => 'avatar',
    'effects' => array(
      14 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 64,
          'height' => 64,
          'retinafy' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: blog_landscape.
  $styles['blog_landscape'] = array(
    'label' => 'blog landscape',
    'effects' => array(
      16 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 450,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: client_logo.
  $styles['client_logo'] = array(
    'label' => 'Client Logo',
    'effects' => array(
      21 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 200,
          'height' => 80,
        ),
        'weight' => 1,
      ),
      22 => array(
        'name' => 'image_desaturate',
        'data' => array(),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: member_avatar.
  $styles['member_avatar'] = array(
    'label' => 'member avatar',
    'effects' => array(
      19 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 480,
          'height' => 480,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: portfolio_landscape_full.
  $styles['portfolio_landscape_full'] = array(
    'label' => 'Portfolio Landscape Full',
    'effects' => array(
      28 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1140,
          'height' => '',
          'upscale' => 1,
          'retinafy' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: portfolio_portrait_full.
  $styles['portfolio_portrait_full'] = array(
    'label' => 'Portfolio portrait full',
    'effects' => array(
      29 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 768,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function zenon_features_node_info() {
  $items = array(
    'article' => array(
      'name' => t('Article'),
      'base' => 'node_content',
      'description' => t('Use <em>articles</em> for time-sensitive content like news, press releases or blog posts.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'blogs' => array(
      'name' => t('Blogs'),
      'base' => 'node_content',
      'description' => t('Blog pages'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'client' => array(
      'name' => t('Client'),
      'base' => 'node_content',
      'description' => t('All Clients'),
      'has_title' => '1',
      'title_label' => t('Company Name'),
      'help' => '',
    ),
    'faq' => array(
      'name' => t('FAQ'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Question'),
      'help' => '',
    ),
    'node_block' => array(
      'name' => t('Node Block'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Basic page'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'portfolio' => array(
      'name' => t('Portfolio'),
      'base' => 'node_content',
      'description' => t('Portfolio Contents'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'pricing_plan' => array(
      'name' => t('Pricing Plan'),
      'base' => 'node_content',
      'description' => t('Pricing Plans for your website'),
      'has_title' => '1',
      'title_label' => t('Plan name'),
      'help' => '',
    ),
    'team_members' => array(
      'name' => t('Team Members'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'testimonials' => array(
      'name' => t('Testimonials'),
      'base' => 'node_content',
      'description' => t('What customers say about you'),
      'has_title' => '1',
      'title_label' => t('Customer/Company'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
