<?php
/**
 * @file
 * zenon_features.features.node_export_features.inc
 */

/**
 * Implements hook_node_export_features_default().
 */
function zenon_features_node_export_features_default() {
  $node_export = array(
  'code_string' => 'array(
  (object) array(
      \'vid\' => \'550\',
      \'uid\' => \'1\',
      \'title\' => \'Contact Informations\',
      \'log\' => \'\',
      \'status\' => \'1\',
      \'comment\' => \'1\',
      \'promote\' => \'0\',
      \'sticky\' => \'0\',
      \'vuuid\' => \'25ea5510-9f81-4ea0-8ac0-452af2e2a2df\',
      \'nid\' => \'550\',
      \'type\' => \'node_block\',
      \'language\' => \'und\',
      \'created\' => \'1414015683\',
      \'changed\' => \'1414016409\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'uuid\' => \'2d3f7098-0bc3-4ffa-b6e6-90c05d08f963\',
      \'revision_timestamp\' => \'1414016409\',
      \'revision_uid\' => \'1\',
      \'body\' => array(
        \'und\' => array(
          array(
            \'value\' => "<header>\\r\\n\\t<h4>\\r\\n\\t\\t<strong>Contact</strong> Informations\\r\\n\\t</h4>\\r\\n</header>\\r\\n\\r\\n<p>\\r\\n\\t<span style=\\"color:#696969;\\"><strong>Main Office:</strong></span><br />\\r\\n\\t453 Shinn Street<br />\\r\\n\\tNew York, NY 10011\\r\\n</p>\\r\\n\\r\\n<p>\\r\\n\\t<span style=\\"color:#696969;\\"><strong>Phone:</strong></span> 1.456.789.0123<br />\\r\\n\\t<span style=\\"color:#696969;\\"><strong><span style=\\"line-height: 1.6em;\\">Fax: </span></strong></span>1.456.789.0129<br />\\r\\n\\t<span style=\\"line-height: 1.6em;\\"><span style=\\"color:#696969;\\"><strong>Email:</strong></span> <a href=\\"mailto:hello@themebiotic.com\\">hello@themebiotic.com</a></span><br />\\r\\n\\t<span style=\\"line-height: 1.6em;\\"><span style=\\"color:#696969;\\"><strong>Web:</strong></span> <a href=\\"http://Themebiotic.com\\">Themebiotic.com</a></span>\\r\\n</p>\\r\\n",
            \'summary\' => \'\',
            \'format\' => \'full_html\',
            \'safe_value\' => "<header><h4>\\n\\t\\t<strong>Contact</strong> Informations\\n\\t</h4>\\n</header><p>\\n\\t<span style=\\"color:#696969;\\"><strong>Main Office:</strong></span><br />\\n\\t453 Shinn Street<br />\\n\\tNew York, NY 10011\\n</p>\\n\\n<p>\\n\\t<span style=\\"color:#696969;\\"><strong>Phone:</strong></span> 1.456.789.0123<br /><span style=\\"color:#696969;\\"><strong><span style=\\"line-height: 1.6em;\\">Fax: </span></strong></span>1.456.789.0129<br /><span style=\\"line-height: 1.6em;\\"><span style=\\"color:#696969;\\"><strong>Email:</strong></span> <a href=\\"mailto:hello@themebiotic.com\\">hello@themebiotic.com</a></span><br /><span style=\\"line-height: 1.6em;\\"><span style=\\"color:#696969;\\"><strong>Web:</strong></span> <a href=\\"http://Themebiotic.com\\">Themebiotic.com</a></span>\\n</p>\\n",
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'cid\' => \'0\',
      \'last_comment_timestamp\' => \'1414015683\',
      \'last_comment_name\' => NULL,
      \'last_comment_uid\' => \'1\',
      \'comment_count\' => \'0\',
      \'nodeblock\' => array(
        \'nid\' => \'550\',
        \'enabled\' => \'1\',
        \'machine_name\' => \'550\',
        \'block_title\' => \'<none>\',
        \'view_mode\' => \'full\',
        \'node_link\' => \'0\',
        \'comment_link\' => \'0\',
        \'translation_fallback\' => \'0\',
      ),
      \'name\' => \'admin\',
      \'picture\' => \'991\',
      \'data\' => \'a:6:{s:16:"ckeditor_default";s:1:"t";s:20:"ckeditor_show_toggle";s:1:"t";s:14:"ckeditor_width";s:4:"100%";s:13:"ckeditor_lang";s:2:"en";s:18:"ckeditor_auto_lang";s:1:"t";s:7:"contact";i:0;}\',
      \'path\' => array(
        \'pid\' => \'462\',
        \'source\' => \'node/550\',
        \'alias\' => \'content/contact-informations\',
        \'language\' => \'und\',
      ),
      \'menu\' => NULL,
      \'node_export_drupal_version\' => \'7\',
    ),
  (object) array(
      \'vid\' => \'546\',
      \'uid\' => \'1\',
      \'title\' => \'User Login Promo \',
      \'log\' => \'\',
      \'status\' => \'1\',
      \'comment\' => \'1\',
      \'promote\' => \'0\',
      \'sticky\' => \'0\',
      \'vuuid\' => \'5d93889a-7706-45ef-a232-7c9f78e661f8\',
      \'nid\' => \'546\',
      \'type\' => \'node_block\',
      \'language\' => \'und\',
      \'created\' => \'1414001181\',
      \'changed\' => \'1414001604\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'uuid\' => \'760dc86a-41e5-4eb2-971d-c8854a7b59cd\',
      \'revision_timestamp\' => \'1414001604\',
      \'revision_uid\' => \'1\',
      \'body\' => array(
        \'und\' => array(
          array(
            \'value\' => "<p>\\r\\n\\tUnam incolunt Belgae, aliam Aquitani, tertiam.\\r\\n</p>\\r\\n\\r\\n<p>\\r\\n\\tQuo usque tandem abutere, Catilina, patientia nostra? Ut enim ad minim veniam, quis nostrud exercitation. Ut enim ad minim veniam, quis nostrud exercitation.\\r\\n</p>\\r\\n",
            \'summary\' => \'\',
            \'format\' => \'full_html\',
            \'safe_value\' => "<p>\\n\\tUnam incolunt Belgae, aliam Aquitani, tertiam.\\n</p>\\n\\n<p>\\n\\tQuo usque tandem abutere, Catilina, patientia nostra? Ut enim ad minim veniam, quis nostrud exercitation. Ut enim ad minim veniam, quis nostrud exercitation.\\n</p>\\n",
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'cid\' => \'0\',
      \'last_comment_timestamp\' => \'1414001181\',
      \'last_comment_name\' => NULL,
      \'last_comment_uid\' => \'1\',
      \'comment_count\' => \'0\',
      \'nodeblock\' => array(
        \'nid\' => \'546\',
        \'enabled\' => \'1\',
        \'machine_name\' => \'546\',
        \'block_title\' => \'User Login Promo \',
        \'view_mode\' => \'full\',
        \'node_link\' => \'0\',
        \'comment_link\' => \'0\',
        \'translation_fallback\' => \'0\',
      ),
      \'name\' => \'admin\',
      \'picture\' => \'991\',
      \'data\' => \'a:6:{s:16:"ckeditor_default";s:1:"t";s:20:"ckeditor_show_toggle";s:1:"t";s:14:"ckeditor_width";s:4:"100%";s:13:"ckeditor_lang";s:2:"en";s:18:"ckeditor_auto_lang";s:1:"t";s:7:"contact";i:0;}\',
      \'path\' => array(
        \'pid\' => \'454\',
        \'source\' => \'node/546\',
        \'alias\' => \'content/user-login-promo\',
        \'language\' => \'und\',
      ),
      \'menu\' => NULL,
      \'node_export_drupal_version\' => \'7\',
    ),
  (object) array(
      \'vid\' => \'428\',
      \'uid\' => \'1\',
      \'title\' => \'Footer Style 1\',
      \'log\' => \'\',
      \'status\' => \'1\',
      \'comment\' => \'1\',
      \'promote\' => \'0\',
      \'sticky\' => \'0\',
      \'vuuid\' => \'7652c864-de33-4a9d-95da-cf19673b363c\',
      \'nid\' => \'428\',
      \'type\' => \'node_block\',
      \'language\' => \'und\',
      \'created\' => \'1410978057\',
      \'changed\' => \'1413725515\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'uuid\' => \'7b0d9090-d142-421b-b46c-a2ea9a3c51d5\',
      \'revision_timestamp\' => \'1413725515\',
      \'revision_uid\' => \'1\',
      \'body\' => array(
        \'und\' => array(
          array(
            \'value\' => "<nav class=\\"footer-wrapper\\">\\r\\n\\t<section class=\\"col-md-3\\">\\r\\n\\t\\t<h3 class=\\"block-title h5\\">\\r\\n\\t\\t\\tPRODUCT\\r\\n\\t\\t</h3>\\r\\n\\r\\n\\t\\t<ul class=\\"list-unstyled\\">\\r\\n\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t<a href=\\"/features\\" title=\\"\\">Features</a>\\r\\n\\t\\t\\t</li>\\r\\n\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t<a href=\\"/pricing-plans\\" title=\\"Plans and Billing\\">Pricing</a>\\r\\n\\t\\t\\t</li>\\r\\n\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t<a href=\\"https://api.themebiotic.com\\" target=\\"_blank\\" title=\\"\\">API</a>\\r\\n\\t\\t\\t</li>\\r\\n\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t<a href=\\"http://twitter.com/themebiotic\\" target=\\"_blank\\" title=\\"\\">Status</a>\\r\\n\\t\\t\\t</li>\\r\\n\\t\\t</ul>\\r\\n\\t</section>\\r\\n\\r\\n\\t<section class=\\"col-md-3\\">\\r\\n\\t\\t<h3 class=\\"block-title h5\\">\\r\\n\\t\\t\\tCOMPANY\\r\\n\\t\\t</h3>\\r\\n\\r\\n\\t\\t<ul class=\\"list-unstyled\\">\\r\\n\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t<a href=\\"/about-us\\" title=\\"About us\\">About us</a>\\r\\n\\t\\t\\t</li>\\r\\n\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t<a href=\\"/team\\" title=\\"Our Team\\">Team</a>\\r\\n\\t\\t\\t</li>\\r\\n\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t<a href=\\"/clients\\" title=\\"Our clients\\">Clients</a>\\r\\n\\t\\t\\t</li>\\r\\n\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t<a href=\\"#\\" title=\\"Press\\">Press</a>\\r\\n\\t\\t\\t</li>\\r\\n\\t\\t</ul>\\r\\n\\t</section>\\r\\n\\r\\n\\t<section class=\\"col-md-3\\">\\r\\n\\t\\t<h3 class=\\"block-title h5\\">\\r\\n\\t\\t\\tKEEP IN TOUCH\\r\\n\\t\\t</h3>\\r\\n\\r\\n\\t\\t<ul class=\\"list-unstyled\\">\\r\\n\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t<a href=\\"/blogs\\" title=\\"Read about news and updates on our blog\\">Our blog</a>\\r\\n\\t\\t\\t</li>\\r\\n\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t<a href=\\"http://twitter.com/themebiotic\\" target=\\"_blank\\" title=\\"Read more on Twitter\\"><span class=\\"fa fa-twitter\\"></span>&nbsp; Twitter</a>\\r\\n\\t\\t\\t</li>\\r\\n\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t<a href=\\"http://facebook.com/Themebiotic\\" target=\\"_blank\\" title=\\"Like us on Facebook\\"><span class=\\"fa fa-facebook\\"></span>&nbsp; Facebook</a>\\r\\n\\t\\t\\t</li>\\r\\n\\t\\t</ul>\\r\\n\\t</section>\\r\\n\\r\\n\\t<section class=\\"col-md-3\\">\\r\\n\\t\\t<h3 class=\\"block-title h5\\">\\r\\n\\t\\t\\tGET ANSWERS\\r\\n\\t\\t</h3>\\r\\n\\r\\n\\t\\t<ul class=\\"list-unstyled\\">\\r\\n\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t<a href=\\"/faq\\" title=\\"Zenon knowledgebase\\">Knowledgebase</a>\\r\\n\\t\\t\\t</li>\\r\\n\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t<a href=\\"/contact\\" title=\\"Contact\\">Contact us</a>\\r\\n\\t\\t\\t</li>\\r\\n\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t<a href=\\"/faq-search\\" title=\\"Zenon documentation\\">How can we help?</a>\\r\\n\\t\\t\\t</li>\\r\\n\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t<a href=\\"#\\" title=\\"Privacy Policy\\">Privacy policy</a>\\r\\n\\t\\t\\t</li>\\r\\n\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t<a href=\\"#\\" title=\\"Terms of Service\\">Terms of use</a>\\r\\n\\t\\t\\t</li>\\r\\n\\t\\t</ul>\\r\\n\\t</section>\\r\\n</nav>\\r\\n",
            \'summary\' => \'\',
            \'format\' => \'full_html\',
            \'safe_value\' => "<nav class=\\"footer-wrapper\\"><section class=\\"col-md-3\\"><h3 class=\\"block-title h5\\">\\n\\t\\t\\tPRODUCT\\n\\t\\t</h3>\\n\\n\\t\\t<ul class=\\"list-unstyled\\"><li>\\n\\t\\t\\t\\t<a href=\\"/features\\" title=\\"\\">Features</a>\\n\\t\\t\\t</li>\\n\\t\\t\\t<li>\\n\\t\\t\\t\\t<a href=\\"/pricing-plans\\" title=\\"Plans and Billing\\">Pricing</a>\\n\\t\\t\\t</li>\\n\\t\\t\\t<li>\\n\\t\\t\\t\\t<a href=\\"https://api.themebiotic.com\\" target=\\"_blank\\" title=\\"\\">API</a>\\n\\t\\t\\t</li>\\n\\t\\t\\t<li>\\n\\t\\t\\t\\t<a href=\\"http://twitter.com/themebiotic\\" target=\\"_blank\\" title=\\"\\">Status</a>\\n\\t\\t\\t</li>\\n\\t\\t</ul></section><section class=\\"col-md-3\\"><h3 class=\\"block-title h5\\">\\n\\t\\t\\tCOMPANY\\n\\t\\t</h3>\\n\\n\\t\\t<ul class=\\"list-unstyled\\"><li>\\n\\t\\t\\t\\t<a href=\\"/about-us\\" title=\\"About us\\">About us</a>\\n\\t\\t\\t</li>\\n\\t\\t\\t<li>\\n\\t\\t\\t\\t<a href=\\"/team\\" title=\\"Our Team\\">Team</a>\\n\\t\\t\\t</li>\\n\\t\\t\\t<li>\\n\\t\\t\\t\\t<a href=\\"/clients\\" title=\\"Our clients\\">Clients</a>\\n\\t\\t\\t</li>\\n\\t\\t\\t<li>\\n\\t\\t\\t\\t<a href=\\"#\\" title=\\"Press\\">Press</a>\\n\\t\\t\\t</li>\\n\\t\\t</ul></section><section class=\\"col-md-3\\"><h3 class=\\"block-title h5\\">\\n\\t\\t\\tKEEP IN TOUCH\\n\\t\\t</h3>\\n\\n\\t\\t<ul class=\\"list-unstyled\\"><li>\\n\\t\\t\\t\\t<a href=\\"/blogs\\" title=\\"Read about news and updates on our blog\\">Our blog</a>\\n\\t\\t\\t</li>\\n\\t\\t\\t<li>\\n\\t\\t\\t\\t<a href=\\"http://twitter.com/themebiotic\\" target=\\"_blank\\" title=\\"Read more on Twitter\\"><span class=\\"fa fa-twitter\\"></span>  Twitter</a>\\n\\t\\t\\t</li>\\n\\t\\t\\t<li>\\n\\t\\t\\t\\t<a href=\\"http://facebook.com/Themebiotic\\" target=\\"_blank\\" title=\\"Like us on Facebook\\"><span class=\\"fa fa-facebook\\"></span>  Facebook</a>\\n\\t\\t\\t</li>\\n\\t\\t</ul></section><section class=\\"col-md-3\\"><h3 class=\\"block-title h5\\">\\n\\t\\t\\tGET ANSWERS\\n\\t\\t</h3>\\n\\n\\t\\t<ul class=\\"list-unstyled\\"><li>\\n\\t\\t\\t\\t<a href=\\"/faq\\" title=\\"Zenon knowledgebase\\">Knowledgebase</a>\\n\\t\\t\\t</li>\\n\\t\\t\\t<li>\\n\\t\\t\\t\\t<a href=\\"/contact\\" title=\\"Contact\\">Contact us</a>\\n\\t\\t\\t</li>\\n\\t\\t\\t<li>\\n\\t\\t\\t\\t<a href=\\"/faq-search\\" title=\\"Zenon documentation\\">How can we help?</a>\\n\\t\\t\\t</li>\\n\\t\\t\\t<li>\\n\\t\\t\\t\\t<a href=\\"#\\" title=\\"Privacy Policy\\">Privacy policy</a>\\n\\t\\t\\t</li>\\n\\t\\t\\t<li>\\n\\t\\t\\t\\t<a href=\\"#\\" title=\\"Terms of Service\\">Terms of use</a>\\n\\t\\t\\t</li>\\n\\t\\t</ul></section></nav>",
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'cid\' => \'0\',
      \'last_comment_timestamp\' => \'1410978057\',
      \'last_comment_name\' => NULL,
      \'last_comment_uid\' => \'1\',
      \'comment_count\' => \'0\',
      \'nodeblock\' => array(
        \'nid\' => \'428\',
        \'enabled\' => \'1\',
        \'machine_name\' => \'428\',
        \'block_title\' => \'<none>\',
        \'view_mode\' => \'full\',
        \'node_link\' => \'0\',
        \'comment_link\' => \'0\',
        \'translation_fallback\' => \'0\',
      ),
      \'name\' => \'admin\',
      \'picture\' => \'991\',
      \'data\' => \'a:6:{s:16:"ckeditor_default";s:1:"t";s:20:"ckeditor_show_toggle";s:1:"t";s:14:"ckeditor_width";s:4:"100%";s:13:"ckeditor_lang";s:2:"en";s:18:"ckeditor_auto_lang";s:1:"t";s:7:"contact";i:0;}\',
      \'path\' => array(
        \'pid\' => \'97\',
        \'source\' => \'node/428\',
        \'alias\' => \'content/footer-style-1\',
        \'language\' => \'und\',
      ),
      \'menu\' => NULL,
      \'node_export_drupal_version\' => \'7\',
    ),
  (object) array(
      \'vid\' => \'547\',
      \'uid\' => \'1\',
      \'title\' => \'User Login Promo First\',
      \'log\' => \'\',
      \'status\' => \'1\',
      \'comment\' => \'1\',
      \'promote\' => \'0\',
      \'sticky\' => \'0\',
      \'vuuid\' => \'3d280720-5478-4817-b3ef-dc1b99cda3f6\',
      \'nid\' => \'547\',
      \'type\' => \'node_block\',
      \'language\' => \'und\',
      \'created\' => \'1414002171\',
      \'changed\' => \'1414009602\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'uuid\' => \'b9e8b60f-9393-472f-bd68-f0d1a3f7719d\',
      \'revision_timestamp\' => \'1414009602\',
      \'revision_uid\' => \'1\',
      \'body\' => array(
        \'und\' => array(
          array(
            \'value\' => "<h4>\\r\\n\\t<strong>Social</strong> Sharing\\r\\n</h4>\\r\\n\\r\\n<p>\\r\\n\\tCurabitur blandit tempus ardua ridiculus sed magna. Quisque ut dolor gravida, placerat libero vel, euismod.\\r\\n</p>\\r\\n\\r\\n<p>\\r\\n\\t<img alt=\\"\\" class=\\"img-responsive\\" src=\\"/sites/default/files/buttons_03.png\\" />\\r\\n</p>\\r\\n",
            \'summary\' => \'\',
            \'format\' => \'full_html\',
            \'safe_value\' => "<h4>\\n\\t<strong>Social</strong> Sharing\\n</h4>\\n\\n<p>\\n\\tCurabitur blandit tempus ardua ridiculus sed magna. Quisque ut dolor gravida, placerat libero vel, euismod.\\n</p>\\n\\n<p>\\n\\t<img alt=\\"\\" class=\\"img-responsive\\" src=\\"/sites/default/files/buttons_03.png\\" /></p>\\n",
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'cid\' => \'0\',
      \'last_comment_timestamp\' => \'1414002171\',
      \'last_comment_name\' => NULL,
      \'last_comment_uid\' => \'1\',
      \'comment_count\' => \'0\',
      \'nodeblock\' => array(
        \'nid\' => \'547\',
        \'enabled\' => \'1\',
        \'machine_name\' => \'547\',
        \'block_title\' => \'<none>\',
        \'view_mode\' => \'full\',
        \'node_link\' => \'0\',
        \'comment_link\' => \'0\',
        \'translation_fallback\' => \'0\',
      ),
      \'name\' => \'admin\',
      \'picture\' => \'991\',
      \'data\' => \'a:6:{s:16:"ckeditor_default";s:1:"t";s:20:"ckeditor_show_toggle";s:1:"t";s:14:"ckeditor_width";s:4:"100%";s:13:"ckeditor_lang";s:2:"en";s:18:"ckeditor_auto_lang";s:1:"t";s:7:"contact";i:0;}\',
      \'path\' => array(
        \'pid\' => \'455\',
        \'source\' => \'node/547\',
        \'alias\' => \'content/user-login-promo-first\',
        \'language\' => \'und\',
      ),
      \'menu\' => NULL,
      \'node_export_drupal_version\' => \'7\',
    ),
)',
);
  return $node_export;
}
