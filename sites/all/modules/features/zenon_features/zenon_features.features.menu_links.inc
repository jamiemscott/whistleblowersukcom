<?php
/**
 * @file
 * zenon_features.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function zenon_features_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_blogs:blogs
  $menu_links['main-menu_blogs:blogs'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'blogs',
    'router_path' => 'blogs',
    'link_title' => 'Blogs',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'main-menu_blogs:blogs',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_classic-blog-page-em-classnew-badge1-columnem:blogs
  $menu_links['main-menu_classic-blog-page-em-classnew-badge1-columnem:blogs'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'blogs',
    'router_path' => 'blogs',
    'link_title' => 'Classic Blog Page <em class="new-badge">1 column</em>',
    'options' => array(
      'attributes' => array(),
      'html' => 1,
      'identifier' => 'main-menu_classic-blog-page-em-classnew-badge1-columnem:blogs',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_blogs:blogs',
  );
  // Exported menu link: main-menu_clients:clients
  $menu_links['main-menu_clients:clients'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'clients',
    'router_path' => 'clients',
    'link_title' => 'Clients',
    'options' => array(
      'icon' => array(
        'icon' => '',
        'position' => 'title_before',
        'title_wrapper_element' => 'span',
        'title_wrapper_class' => 'title',
        'breadcrumb' => 0,
        'title_wrapper' => 0,
      ),
      'attributes' => array(),
      'identifier' => 'main-menu_clients:clients',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'main-menu_pages:<front>',
  );
  // Exported menu link: main-menu_contact:contact
  $menu_links['main-menu_contact:contact'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'contact',
    'router_path' => 'contact',
    'link_title' => 'Contact',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'main-menu_contact:contact',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: main-menu_home:<front>
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'icon' => array(
        'icon' => '',
        'position' => 'title_before',
        'title_wrapper_element' => 'span',
        'title_wrapper_class' => 'title',
        'breadcrumb' => 0,
        'title_wrapper' => 0,
      ),
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_pages:<front>
  $menu_links['main-menu_pages:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Pages',
    'options' => array(
      'icon' => array(
        'icon' => '',
        'position' => 'title_before',
        'title_wrapper_element' => 'span',
        'title_wrapper_class' => 'title',
        'breadcrumb' => 0,
        'title_wrapper' => 0,
      ),
      'attributes' => array(),
      'identifier' => 'main-menu_pages:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_pricing-plans:pricing-plans
  $menu_links['main-menu_pricing-plans:pricing-plans'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'pricing-plans',
    'router_path' => 'pricing-plans',
    'link_title' => 'Pricing plans',
    'options' => array(
      'icon' => array(
        'icon' => '',
        'position' => 'title_before',
        'title_wrapper_element' => 'span',
        'title_wrapper_class' => 'title',
        'breadcrumb' => 0,
        'title_wrapper' => 0,
      ),
      'attributes' => array(),
      'identifier' => 'main-menu_pricing-plans:pricing-plans',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
    'parent_identifier' => 'main-menu_pages:<front>',
  );
  // Exported menu link: main-menu_team:team
  $menu_links['main-menu_team:team'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'team',
    'router_path' => 'team',
    'link_title' => 'Team',
    'options' => array(
      'icon' => array(
        'icon' => '',
        'position' => 'title_before',
        'title_wrapper_element' => 'span',
        'title_wrapper_class' => 'title',
        'breadcrumb' => 0,
        'title_wrapper' => 0,
      ),
      'attributes' => array(),
      'identifier' => 'main-menu_team:team',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_pages:<front>',
  );
  // Exported menu link: main-menu_testimonials:testimonials
  $menu_links['main-menu_testimonials:testimonials'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'testimonials',
    'router_path' => 'testimonials',
    'link_title' => 'Testimonials',
    'options' => array(
      'icon' => array(
        'icon' => '',
        'position' => 'title_before',
        'title_wrapper_element' => 'span',
        'title_wrapper_class' => 'title',
        'breadcrumb' => 0,
        'title_wrapper' => 0,
      ),
      'attributes' => array(),
      'identifier' => 'main-menu_testimonials:testimonials',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -43,
    'customized' => 1,
    'parent_identifier' => 'main-menu_pages:<front>',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Blogs');
  t('Classic Blog Page <em class="new-badge">1 column</em>');
  t('Clients');
  t('Contact');
  t('Home');
  t('Pages');
  t('Pricing plans');
  t('Team');
  t('Testimonials');


  return $menu_links;
}
